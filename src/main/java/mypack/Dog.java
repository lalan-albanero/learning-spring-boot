package mypack;

import org.springframework.stereotype.Component;

@Component
public class Dog {
    public void sound() {
        System.out.println("dog barks...");
    }
}
