package com.firstapp.firstspringapp;

import java.util.Date;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
@ComponentScan(basePackages = { "mypack" })
public class Myconfig {
    @Bean("student1")
    @Lazy
    public Student getStudent() {
        System.out.println("creating Student1.");
        return new Student();
    }

    @Bean("student2")
    @Lazy
    public Student createStudent() {
        System.out.println("creating Student2.");
        return new Student();
    }
    // @Bean
    // public Date getDate(){
    // System.out.println("creating date.");
    // return new Date();
    // }
}
