package com.firstapp.firstspringapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.firstapp.firstspringapp.Student;

@Controller
public class mycontroller {
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    @ResponseBody
    public Student home(@RequestBody Student st) {
        st.show();
        System.out.println("in home");
        return st;
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public String user(@PathVariable("userId") Integer userId) {

        return String.valueOf(userId);
    }
}
