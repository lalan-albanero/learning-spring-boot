package com.firstapp.firstspringapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import mypack.*;

@SpringBootApplication
@RestController
public class FirstspringappApplication {
	@Autowired
	@Qualifier("student2")
	private Student S1;
	@Autowired
	private Dog d1;
	

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(FirstspringappApplication.class, args);
		Alien a = context.getBean(Alien.class);
		a.show();
	}

	@GetMapping("/")
	public String get() {
		this.S1.show();
		this.d1.sound();
		return "Hi Spring boot from vscode.";

	}

}
