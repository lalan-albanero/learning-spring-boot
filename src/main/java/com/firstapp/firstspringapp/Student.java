package com.firstapp.firstspringapp;

public class Student {
    String name;

    public Student() {
        
    }

    public void show() {
        System.out.println("in " + this.name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
