package com.firstapp.firstspringapp;

import org.springframework.stereotype.Component;

@Component
public class Alien {
    private String name;

    public Alien() {
        System.out.println("Object created....");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void show() {
        System.out.println("in show..");
    }

}
